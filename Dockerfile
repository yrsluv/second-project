FROM node:16-alpine as dependencies
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install --include=dev

FROM node:16-alpine as builder
WORKDIR /app
COPY . .

COPY --from=dependencies /app/node_modules ./node_modules
RUN npm run build

FROM node:16-alpine as runner
WORKDIR /app
ENV NODE_ENV production
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/node_modules ./node_modules


